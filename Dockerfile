FROM tomcat:8.5-jdk8-openjdk

RUN mkdir -p /opt/tomcat8/webapps
RUN mkdir /usr/local/openjdk-8/jre/lib/fonts

COPY fonts/* /usr/local/openjdk-8/jre/lib/fonts/

RUN sed -i 's/webapps/\/opt\/tomcat8\/webapps/g' /usr/local/tomcat/conf/server.xml